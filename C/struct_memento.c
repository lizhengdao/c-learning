//
//  struct_memento.c
//  C
//
//  Created by bullhead on 11/22/20.
//

#include <stdio.h>
#include <stdlib.h>

struct point {
    int x;
    int y;
};

int main(int argc,char ** argv){
    struct point *p=(struct point*)malloc(sizeof(struct point));
    p->x=10;
    p->y=10;
    printf("(x,y) = (%d,%d)\n",p->x,p->y);
    printf("Size of p is %lu\n",sizeof(p));
    free(p);
    return 0;
}
