//
//  split_string.c
//  C
//
//  Created by bullhead on 12/3/20.
//



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char** split(char *data,char token){
    char current=data[0];
    char **to_return;
    int count=0;
    int split_count=1;
    char *current_split=(char *)calloc(0, sizeof(char));
    long len=strlen(data);
    for(int i=0;i<len;i++){
        if (i+1==len) {
            //if string finishing then do not look for token and copy whatever we have
            current_split=realloc(current_split, sizeof(char)*(count+1));
            current=data[i];
            if (current!=token) {
                current_split[count++]=current;
            }
            current=token;
        }
        
        if (current==token) {
            if (split_count==1) {
                to_return=(char **)calloc(split_count, sizeof(char *));
            }else{
                to_return=realloc(to_return, sizeof(char *)*split_count);
            }
            
            to_return[split_count-1]=(char *)calloc(strlen(current_split), sizeof(char));
            strcpy(to_return[split_count-1],current_split);
            split_count++;
            free(current_split);
            
            //copy current char, which is after token
            count=1;
            current=data[i];
            current_split=(char *)calloc(1, sizeof(char));
            current_split[0]=current;
            continue;
        }
        current=data[i];
        if (current!=token) {
            current_split=realloc(current_split, sizeof(char)*(count+1));
            current_split[count++]=current;
        }
    }
    return to_return;
}
int main(int argc,char **argv){
    char *data="I am Osama Bin Omar";
    char **result=split(data, ' ');
    for (int i=0; i<5; i++) {
        printf("[%d]=%s\n",i,result[i]);
    }
    
}
