//
//  type_definition.c
//  C
//
//  Created by bullhead on 11/22/20.
//

#include <stdio.h>
typedef int Distance;
typedef char* String;

typedef struct node{
    int value;
    struct node *next;
} Node;
//struct typedef
typedef struct point* Point;
struct point{
    int x;
    int y;
};

int two_times(int x){
    return x*2;
}

//function typedef
typedef int (*FPII)(int);
FPII the_func=two_times;

int main(int argc,char **argv){
    the_func(100);
}

