//
//  linked_list.c
//  C
//
//  Created by bullhead on 11/22/20.
//
#include <stdio.h>
#include <stdlib.h>

struct node {
    int value;
    struct node *next;
};

int main(int argc,char** argv){
    struct node *head=(struct node*)malloc(sizeof(struct node));
    head->value=10;
    head->next=(struct node*)malloc(sizeof(struct node));
    head->next->value=15;
    head->next->next=NULL;
    for(struct node *p=head;p!=NULL;p=p->next){
        printf("value is %d\n",p->value);
    }
    free(head->next);
    free(head);
    return 0;
}
