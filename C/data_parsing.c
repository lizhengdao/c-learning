//
//  data_parsing.c
//  C
//
//  Created by bullhead on 11/24/20.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc,char **argv){
    //read file
    char * buffer = 0;
    long length;
    FILE * f = fopen ("/Users/bullhead/IdeaProjects/C/C/data", "r");
    
    if (f){
        fseek (f, 0, SEEK_END);
        length = ftell (f);
        fseek (f, 0, SEEK_SET);
        buffer = malloc (length);
        if (buffer){
            fread (buffer, 1, length, f);
        }
        fclose (f);
    }else{
        printf("failed to read file.\n");
    }
    
    if (buffer){
        /*because strtok changes the original string,
         but we do not want it to modify original string so copy original string to new modifiable string
         */
        char modifiable[strlen(buffer)];
        strcpy(modifiable, buffer);
        
        //get first line of data "\n" is newline character.
        char *first_line=strtok(modifiable, "\n");
        printf("First line: %s\n",first_line);
        char *useless=strtok(first_line, ",");
        printf("Useless part: %s\n",useless);
        char *mac_address=strtok(NULL, ",");
        printf("MAC: %s\n",mac_address);
        int rssi=(int)strtol(strtok(NULL, ","), NULL, 10);
        printf("RSSI: %d\n",rssi);
        char *raw=strtok(NULL, ",");
        printf("RAW: %s\n",raw);
        const char sunny[]={raw[24],raw[25],raw[26],raw[27],'\0'};
        printf("SUN: %s\n",sunny);
        if (rssi>80 && strcmp(sunny, "536E")==0) {
            printf("It is possible\n");
        }else{
            printf("It is not possible\n");
        }
        //make sure original data is not changed
        printf("%s\n",buffer);
    }
}
